package ru.tinkoff

import com.typesafe.config.ConfigFactory
import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.{SparkConf, SparkContext}
import ru.tinkoff.drools.engine.{DroolsHelper, RuleFetcher}

import scala.collection.JavaConversions._
import scala.util.Try

/**
 * Created by d.a.martyanov on 01.06.15.
 */
object Application extends DroolsHelper {

  lazy val cfg = ConfigFactory.load

  lazy val sparkHomeDir = cfg.getString("spark.home")
  lazy val outSchema = Try {
    cfg.getString("data.out.schema")
  } getOrElse "drools_sandbox"
  lazy val rulesDir = cfg.getString("driver.rules")

  def main(args: Array[String]): Unit = {
    val target = args.headOption.getOrElse("offers_" + new java.util.Date().getTime)

    val sparkContext = initSparkContext
    val hc = new HiveContext(sparkContext)

    val droolsRules = RuleFetcher.fetchRulesFromJars(rulesDir)

    println("RULES: \n" + droolsRules.mkString("\n"))

    val result = offersRDD(hc, droolsRules)
    saveRecs2Table(hc, target)(result)
  }

  def initSparkContext = {
    val sparkCfg = new SparkConf()
      .setAppName(cfg.getString("driver.name"))
      .setAll(cfg.getObject("spark").map { case (k, v) => k -> v.toString} toSeq)

    new SparkContext(sparkCfg)
  }

}
