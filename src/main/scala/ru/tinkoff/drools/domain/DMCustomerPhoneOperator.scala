package ru.tinkoff.drools.domain

import java.util.Date

import scala.beans.BeanProperty

/**
 * Created by d.a.martyanov on 23.04.15.
 */
case class DMCustomerPhoneOperator(
                                    @BeanProperty phone_operator_nm: String,
                                    @BeanProperty processed_dttm: Date
                                    )
