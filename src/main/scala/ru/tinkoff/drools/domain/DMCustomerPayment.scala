package ru.tinkoff.drools.domain

import java.util.Date

import scala.beans.BeanProperty

/**
 * Created by d.a.martyanov on 23.04.15.
 */
case class DMCustomerPayment(
                              @BeanProperty provider_nm: String,
                              @BeanProperty last_purchase_dttm: Date,
                              @BeanProperty purchase_cnt: Int,
                              @BeanProperty processed_dttm: Date
                              )
