package ru.tinkoff.drools.domain

import java.util.Date

import scala.beans.BeanProperty

/**
 * Created by d.a.martyanov on 23.04.15.
 */
case class DMCustomerInfo(
                           @BeanProperty sms_service_active_flg: Boolean,
                           @BeanProperty birth_dt: Date,
                           @BeanProperty gender_cd: String,
                           @BeanProperty age: Int,
                           @BeanProperty marital_status_cd: String,
                           @BeanProperty processed_dttm: Date
                           )
