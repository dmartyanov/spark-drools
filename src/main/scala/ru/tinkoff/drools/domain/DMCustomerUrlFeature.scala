package ru.tinkoff.drools.domain

import java.util.Date

import scala.beans.BeanProperty

/**
 * Created by d.a.martyanov on 23.04.15.
 */
case class DMCustomerUrlFeature(
                                 @BeanProperty pattern_nm: String,
                                 @BeanProperty last_visit_dttm: Date,
                                 @BeanProperty session_cnt: Int,
                                 @BeanProperty hit_cnt: Int,
                                 @BeanProperty day_visit_cnt: Int,
                                 @BeanProperty processed_dttm: Date
                                 )
