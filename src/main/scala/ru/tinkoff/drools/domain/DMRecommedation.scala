package ru.tinkoff.drools.domain

/**
 * Created by d.a.martyanov on 07.05.15.
 */
case class DMRecommedation(
                            wu_rk: String,
                            offer_id: String
                            )
