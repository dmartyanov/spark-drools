package ru.tinkoff.drools.domain

import java.util.Date

import scala.beans.BeanProperty

/**
 * Created by d.a.martyanov on 23.04.15.
 */
case class DMCustomerISP(
                          @BeanProperty isp_nm: String,
                          @BeanProperty last_visit_dt: Date,
                          @BeanProperty days_cnt: Int,
                          @BeanProperty processed_dttm: Date
                          )
