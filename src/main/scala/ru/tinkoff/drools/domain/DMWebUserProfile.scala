package ru.tinkoff.drools.domain

import scala.beans.BeanProperty

/**
 * Created by d.a.martyanov on 23.04.15.
 */
case class DMWebUserProfile(
                             //@BeanProperty wu_rk: String,
                             @BeanProperty cards: List[DMCustomerCard] = List(),
                             @BeanProperty info: Option[DMCustomerInfo] = None,
                             @BeanProperty isps: List[DMCustomerISP] = List(),
                             @BeanProperty payments: List[DMCustomerPayment] = List(),
                             @BeanProperty phoneOperators: List[DMCustomerPhoneOperator] = List(),
                             @BeanProperty urlFeatures: List[DMCustomerUrlFeature] = List()
                             )
