package ru.tinkoff.drools.domain

import java.util.Date

import scala.beans.BeanProperty

/**
 * Created by d.a.martyanov on 23.04.15.
 */
case class DMCustomerCard(
                           @BeanProperty card_type: String,
                           @BeanProperty issuer_nm: String,
                           @BeanProperty expiration_dt: Date,
                           @BeanProperty processed_dttm: Date
                           )
