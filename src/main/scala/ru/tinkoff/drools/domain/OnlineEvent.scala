package ru.tinkoff.drools.domain

import scala.beans.BeanProperty

/**
 * Created by d.a.martyanov on 15.05.15.
 */
case class OnlineEvent(
                        @BeanProperty id: String,
                        @BeanProperty context: java.util.HashMap[String, String]
                        )
