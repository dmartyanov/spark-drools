package ru.tinkoff.drools.engine

import java.util

import org.kie.api.io.ResourceType
import org.kie.internal.KnowledgeBaseFactory
import org.kie.internal.builder.KnowledgeBuilderFactory
import org.kie.internal.io.ResourceFactory
import ru.tinkoff.drools.domain.DMWebUserProfile

import scala.collection.JavaConverters._

/**
 * Created by d.a.martyanov on 30.04.15.
 */
class DroolsMatcher(rules: List[String]) extends Serializable {

  val cl = ClassLoader.getSystemClassLoader
  val kbuilderConf = KnowledgeBuilderFactory.newKnowledgeBuilderConfiguration(null, cl)
  val kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder(kbuilderConf)

  val kbaseConf = KnowledgeBaseFactory.newKnowledgeBaseConfiguration(null, cl)
  val kbase = KnowledgeBaseFactory.newKnowledgeBase(kbaseConf)

    rules foreach { r =>
      //kbuilder.add(ResourceFactory.newClassPathResource(r), ResourceType.DRL)
      kbuilder.add(ResourceFactory.newByteArrayResource(r.getBytes()), ResourceType.DRL)
    }
  //kbuilder.add(ResourceFactory.newClassPathResource("prod-rules.drl"), ResourceType.DRL)
  //kbuilder.add(ResourceFactory.newClassPathResource("rules/demo-rule1.drl", cl), ResourceType.DRL)

  val pkgs = kbuilder.getKnowledgePackages()
  kbase.addKnowledgePackages(pkgs)

  def firedRules(profile: DMWebUserProfile) = {

    val session = kbase.newStatelessKnowledgeSession
    val offers = new util.ArrayList[String]()
    session.setGlobal("offers", offers)

    val builder = List.newBuilder[AnyRef]
    profile.info.foreach(builder +=)
    builder ++= profile.cards
    builder ++= profile.isps
    builder ++= profile.payments
    builder ++= profile.phoneOperators
    builder ++= profile.urlFeatures

    session.execute(builder.result().asJava)
    offers.asScala
  }

  def rulePath(ruleName: String) = "rules/" + ruleName
}
