package ru.tinkoff.drools.engine

import java.io.File
import java.util.zip.ZipFile

import scala.io.Source
import scala.util.{Try, Success, Failure}

/**
 * Created by d.a.martyanov on 03.06.15.
 */
object RuleFetcher {

  lazy val jarFilter = (f: File) => f.getName.endsWith(".jar")
  lazy val drlFilter = (str: String) => str.endsWith("drl")

  def fetchRuleFromFile(filename: String): String = Source.fromFile(filename, "utf-8").getLines().mkString("\n")

  def fetchRulesFromJars(root: String): List[String] = {

    def recursiveListFiles(f: File): Array[File] = {
      val these = f.listFiles()
      val valid = these.filter(jarFilter)
      valid ++ these.filter(_.isDirectory).flatMap(recursiveListFiles)
    }

    val jars: Seq[File] = Try(new File(root)) filter (_.exists()) match {
      case Success(dir) => recursiveListFiles(dir)
      case Failure(err) =>
        err.printStackTrace()
        Seq()
    }
    println("JARS: \n" + jars.map(_.getName).mkString("\n"))

    var ruleList = List.newBuilder[String]
    jars foreach { jar =>
       Try {
         new ZipFile(jar)
       } map { z =>
        val entries  = z.entries()

        val ls = List.newBuilder[String]
        while (entries.hasMoreElements) { ls += entries.nextElement().getName }

        val iss = ls.result() filter drlFilter map { f =>
          println(s"Processing file: ${jar.getName} -> rule: $f")
          z.getInputStream(z.getEntry(f))
        }
        iss foreach { is =>
          ruleList += Source.fromInputStream(is, "utf-8").getLines().mkString("\n")
        }

       } recover {
         case e: Exception => e.printStackTrace()
       }
    }
    ruleList.result()
  }
}
