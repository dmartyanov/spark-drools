package ru.tinkoff.drools.engine

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.hive.HiveContext
import ru.tinkoff.drools.domain.DMRecommedation

/**
 * Created by d.a.martyanov on 08.06.15.
 */
trait DroolsHelper {

  val outSchema: String

  def offersRDD(hc: HiveContext, rs: List[String]) = {
    val profiles = new HiveAdapter(hc).buildCustomerProfileRDDPaired

    profiles.mapPartitionsWithIndex { case (index, it) =>
      val m = new DroolsMatcher(rs)
      it.flatMap {
        case (wurk, profile) =>
          val res = m.firedRules(profile)
          res.map(id => (wurk, id))
      }
    }
  }

  def saveRecs2Table(hc: HiveContext, tableName: String) = (rdd: RDD[(String, String)]) => {
    val tmpTableName = "rmd_tmp"
    hc.createDataFrame(rdd.map(r => DMRecommedation(r._1, r._2))).registerTempTable(tmpTableName)
    hc.sql(s"drop table $outSchema.$tableName")
    hc.sql(s"create table $outSchema.$tableName as select * from $tmpTableName")
  }
}
