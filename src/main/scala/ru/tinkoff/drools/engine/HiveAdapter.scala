package ru.tinkoff.drools.engine

import com.typesafe.config.ConfigFactory
import org.apache.spark.sql
import org.apache.spark.sql.hive.HiveContext
import ru.tinkoff.drools.domain._

import scala.util.Try

/**
 * Created by d.a.martyanov on 24.04.15.
 */
class HiveAdapter(hc: HiveContext) extends Serializable {

  lazy val cfg = ConfigFactory.load()
  lazy val schema = cfg.getString("data.in.schema")
  lazy val prefix = Try {
    cfg.getString("data.in.prefix")
  } getOrElse "dt"

  lazy val CustomerInfoTable = tableName("customer_info")
  lazy val CustomerCardTable = tableName("customer_card")
  lazy val CustomerISPTable = tableName("customer_isp")
  lazy val CustomerPaymentTable = tableName("customer_payment")
  lazy val CustomerPhoneOpTable = tableName("customer_phone_operator")
  lazy val CustomerURLFeatureTable = tableName("customer_url_feature")

  lazy val ciTable = hc.table(CustomerInfoTable)
  lazy val ccTable = hc.table(CustomerCardTable)
  lazy val cispTable = hc.table(CustomerISPTable)
  lazy val cpTable = hc.table(CustomerPaymentTable)
  lazy val cpoTable = hc.table(CustomerPhoneOpTable)
  lazy val cufTable = hc.table(CustomerURLFeatureTable)


  def buildCustomerProfileRDDPaired = {
    val ciPairs = ciTable.map(r => (r.getString(0), infoFromRow(r))).groupByKey
    val ccPairs = ccTable.map(r => (r.getString(0), cardFromRow(r))).groupByKey
    val cpPairs = cpTable.map(r => (r.getString(0), paymentFromRow(r))).groupByKey
    val cufPairs = cufTable.map(r => (r.getString(0), urlFeature(r))).groupByKey

    val groupedRdd = ciPairs.cogroup(ccPairs, cpPairs, cufPairs)
    groupedRdd.map { case (wurk, (infos, cards, payments, urls)) =>
      (wurk, DMWebUserProfile(
        //wu_rk = wurk,
        info = infos.flatten.headOption,
        cards = cards.flatten.toList,
        payments = payments.flatten.toList,
        urlFeatures = urls.flatten.toList
      )
        )
    }
  }

  def infoFromRow(r: sql.Row) = DMCustomerInfo(
    sms_service_active_flg = r.getInt(1) == 1,
    birth_dt = r.getAs[java.util.Date](2),
    gender_cd = r.getString(3),
    age = Try {
      r.getInt(4)
    } getOrElse 0,
    marital_status_cd = r.getString(5),
    processed_dttm = r.getAs[java.util.Date](6)
  )

  def cardFromRow(r: sql.Row) = DMCustomerCard(
    card_type = r.getString(1),
    issuer_nm = r.getString(2),
    expiration_dt = r.getAs[java.util.Date](3),
    processed_dttm = r.getAs[java.util.Date](4)
  )

  def ispFromRow(r: sql.Row) = DMCustomerISP(
    isp_nm = r.getString(1),
    last_visit_dt = r.getAs[java.util.Date](2),
    days_cnt = r.getInt(3),
    processed_dttm = r.getAs[java.util.Date](4)
  )

  def paymentFromRow(r: sql.Row) = DMCustomerPayment(
    provider_nm = r.getString(1),
    last_purchase_dttm = r.getAs[java.util.Date](2),
    purchase_cnt = r.getInt(3),
    processed_dttm = r.getAs[java.util.Date](4)
  )

  def phoneOperatorFromRow(r: sql.Row) = DMCustomerPhoneOperator(
    phone_operator_nm = r.getString(1),
    processed_dttm = r.getAs[java.util.Date](2)
  )

  def urlFeature(r: sql.Row) = DMCustomerUrlFeature(
    pattern_nm = r.getString(1),
    last_visit_dttm = r.getAs[java.util.Date](2),
    session_cnt = r.getInt(3),
    hit_cnt = r.getInt(4),
    day_visit_cnt = r.getInt(5),
    processed_dttm = r.getAs[java.util.Date](6)
  )

  private def tableName(namePart: String) = s"$schema.${prefix}_$namePart"

}
